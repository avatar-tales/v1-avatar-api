package fr.avatarreturns.api.utils;

public interface IMessages {

    /**
     * Method to get a message
     * @return the no permission message
     */
    String getNoPermission();

    /**
     * Method to get a message
     * @return the not enough arguments message
     */
    String getNotEnoughArguments();

    /**
     * Method to get a message
     * @return the need player message
     */
    String getNeedPlayer();

    /**
     * Method to get a message
     * @return the need console message
     */
    String getNeedConsole();

    /**
     * Method to get a message
     * @return the command not found message
     */
    String getCommandNotFound();

    /**
     * Method to get a message
     * @return the reflection error message
     */
    String getReflectionError();

    /**
     * Method to get a message
     * @return the command disabled message
     */
    String getCommandDisabled();

}
