package fr.avatarreturns.api.utils;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Skull;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface IUtils {

    /**
     * Method to get an {@link String} of a {@link UUID} (without dashes)
     * from a {@link Player}'s name;
     * @param name the {@link Player}'s name;
     * @return the {@link Optional} of the {@link String} of the {@link Player}'s {@link UUID} without dashes
     */
    Optional<String> getUUID(final String name);

    /**
     * Method to get an {@link UUID} from a {@link Player}'s name
     * @param name the {@link Player}'s name
     * @return the {@link Optional} of the {@link UUID}
     */
    Optional<UUID> getRealUUID(final String name);

    /**
     * Method to convert a {@link String} of a {@link UUID} (without dashes) to a
     * real {@link UUID} (with dashes)
     * @param uuid the {@link String};
     * @return the {@link Optional} of the {@link UUID}
     */
    Optional<UUID> getFullUUID(final String uuid);

    /**
     * Method to get {@link Player}'s skin
     * @param player the {@link Player}
     * @return the {@link Optional} of the {@link String}[] which contains the value then the signature.
     */
    Optional<String[]> getSkin(final Player player);

    /**
     * Method to get {@link Player}'s skin from his name
     * @param name the {@link Player}'s name
     * @return the {@link Optional} of the {@link String}[] which contains the value then the signature.
     */
    Optional<String[]> getSkin(final String name);

    /**
     * Method to get an {@link ItemStack} from a {@link Material}
     * @param material the {@link Material}
     * @return the {@link ItemStack}
     */
    ItemStack getItem(final Material material);

    /**
     * Method to get an {@link ItemStack} from a {@link Material} and a name
     * @param material the {@link Material}
     * @param name the {@link String} name
     * @return the {@link ItemStack}
     */
    ItemStack getItem(final Material material, final String name);

    /**
     * Method to get an {@link ItemStack} from a {@link Material} and a quantity
     * @param material the {@link Material}
     * @param amount the {@link Integer} amount
     * @return the {@link ItemStack}
     */
    ItemStack getItem(final Material material, final int amount);

    /**
     * Method to get an {@link ItemStack} from a {@link Material} and a lore
     * @param material the {@link Material}
     * @param lore the {@link List} of lore's lines
     * @return the {@link ItemStack}
     */
    ItemStack getItem(final Material material, final List<String> lore);

    /**
     * Method to get an {@link ItemStack} from a {@link Material}, a quantity and a lore
     * @param material the {@link Material}
     * @param amount the {@link Integer} amount
     * @param lore the {@link List} of lore's lines
     * @return the {@link ItemStack}
     */
    ItemStack getItem(final Material material, final int amount, final List<String> lore);

    /**
     * Method to get an {@link ItemStack} from a {@link Material}, a name and a quantity
     * @param material the {@link Material}
     * @param name the {@link String} name
     * @param amount the {@link Integer} amount
     * @return the {@link ItemStack}
     */
    ItemStack getItem(final Material material, final String name, final int amount);

    /**
     * Method to get an {@link ItemStack} from a {@link Material}, a name and a lore
     * @param material the {@link Material}
     * @param name the {@link String} name
     * @param lore the {@link List} of lore's lines
     * @return the {@link ItemStack}
     */
    ItemStack getItem(final Material material, final String name, final List<String> lore);

    /**
     * Method to get an {@link ItemStack} from a {@link Material}, a name, a quantity and a lore
     * @param material the {@link Material}
     * @param name the {@link String} name
     * @param amount the {@link Integer} amount
     * @param lore the {@link List} of lore's lines
     * @return the {@link ItemStack}
     */
    ItemStack getItem(final Material material, final String name, final int amount, final List<String> lore);

    /**
     * Method to get the {@link ItemStack} of {@link Skull}
     * @param texture the texture of the {@link Skull}
     * @return the {@link Optional} of the {@link ItemStack} of {@link Skull}
     */
    Optional<ItemStack> getSkull(final String texture);

    /**
     * Method to get the {@link ItemStack} of {@link Skull}
     * @param uuid the uuid of an {@link org.bukkit.OfflinePlayer}
     * @return the {@link Optional} of the {@link ItemStack} of {@link Skull}
     */
    Optional<ItemStack> getSkull(final UUID uuid);

    /**
     * Method to modify the name of an {@link ItemStack}
     * @param itemStack the {@link ItemStack}
     * @param name the new name
     * @return the modified {@link ItemStack}
     */
    ItemStack modifyItem(final ItemStack itemStack, final String name);

    /**
     * Method to modify the quantity of an {@link ItemStack}
     * @param itemStack the {@link ItemStack}
     * @param amount the new quantity
     * @return the modified {@link ItemStack}
     */
    ItemStack modifyItem(final ItemStack itemStack, final int amount);

    /**
     * Method to modify the lore of an {@link ItemStack}
     * @param itemStack the {@link ItemStack}
     * @param lore the new lore
     * @return the modified {@link ItemStack}
     */
    ItemStack modifyItem(final ItemStack itemStack, final List<String> lore);

    /**
     * Method to modify the name and the quantity of an {@link ItemStack}
     * @param itemStack the {@link ItemStack}
     * @param name the new name
     * @param amount the new quantity
     * @return the modified {@link ItemStack}
     */
    ItemStack modifyItem(final ItemStack itemStack, final String name, final int amount);

    /**
     * Method to modify the name and the lore of an {@link ItemStack}
     * @param itemStack the {@link ItemStack}
     * @param name the new name
     * @param lore the new lore
     * @return the modified {@link ItemStack}
     */
    ItemStack modifyItem(final ItemStack itemStack, final String name, final List<String> lore);

    /**
     * Method to modify the quantity and the lore of an {@link ItemStack}
     * @param itemStack the {@link ItemStack}
     * @param amount the new quantity
     * @param lore the new lore
     * @return the modified {@link ItemStack}
     */
    ItemStack modifyItem(final ItemStack itemStack, final int amount, final List<String> lore);

    /**
     * Method to modify the name, the quantity and the lore of an {@link ItemStack}
     * @param itemStack the {@link ItemStack}
     * @param name the new name
     * @param amount the new quantity
     * @param lore the new lore
     * @return the modified {@link ItemStack}
     */
    ItemStack modifyItem(final ItemStack itemStack, final String name, final int amount, final List<String> lore);

    /**
     * Method to convert a double to string
     * @param doubleValue the double value
     * @return the string representation of the double value
     */
    String convertToString(final double doubleValue);

    /**
     * Method to copy easily a {@link List} into another
     * @param data the dataset to copy
     * @param <T> the {@link Class} of the data
     * @return {@link List} the copied {@link List}
     */
    <T> List<T> copy(final List<T> data);

    /**
     * Method to copy easily a {@link T}[] into a {@link List}
     * @param data the dataset to copy
     * @param <T> the {@link Class} of the data
     * @return {@link List} the copied {@link List}
     */
    <T> List<T> copy(final T[] data);

    /**
     * Method to rotate a {@link Vector} around axis X
     * @param v the {@link Vector}
     * @param angle the angle of rotation
     * @return {@link Vector} the rotated vector
     */
    Vector rotateAroundAxisX(final Vector v, final double angle);

    /**
     * Method to rotate a {@link Vector} around axis Y
     * @param v the {@link Vector}
     * @param angle the angle of rotation
     * @return {@link Vector} the rotated vector
     */
    Vector rotateAroundAxisY(final Vector v, final double angle);

    /**
     * Method to rotate a {@link Vector} around axis Z
     * @param v the {@link Vector}
     * @param angle the angle of rotation
     * @return {@link Vector} the rotated vector
     */
    Vector rotateAroundAxisZ(final Vector v, final double angle);

    /**
     * Method to rotate a {@link Vector} around axis X
     * @param v the {@link Vector}
     * @param cos the cosinus of the angle of rotation
     * @param sin the sinus of the angle of rotation
     * @return {@link Vector} the rotated vector
     */
    Vector rotateAroundAxisX(final Vector v, final double cos, final double sin);

    /**
     * Method to rotate a {@link Vector} around axis Y
     * @param v the {@link Vector}
     * @param cos the cosinus of the angle of rotation
     * @param sin the sinus of the angle of rotation
     * @return {@link Vector} the rotated vector
     */
    Vector rotateAroundAxisY(final Vector v, final double cos, final double sin);

    /**
     * Method to rotate a {@link Vector} around axis Z
     * @param v the {@link Vector}
     * @param cos the cosinus of the angle of rotation
     * @param sin the sinus of the angle of rotation
     * @return {@link Vector} the rotated vector
     */
    Vector rotateAroundAxisZ(final Vector v, final double cos, final double sin);

    /**
     * Method to get the perpendicular {@link Vector} between two others
     * @param u the first {@link Vector}
     * @param v the second {@link Vector}
     * @return the perpendicular {@link Vector}
     */
    Vector perp(final Vector u, final Vector v);

    /**
     * Method to get the projection of a {@link Vector} on another
     * @param onto the {@link Vector}
     * @param u the {@link Vector} to project
     * @return the projected {@link Vector}
     */
    Vector proj(final Vector onto, final Vector u);

    /**
     * Method to create a {@link Vector} from 2 {@link Location}
     * @param start the origin of the {@link Vector}
     * @param end the end of the {@link Vector}
     * @return {@link Vector} the calculated vector
     */
    Vector from(final Location start, final Location end);

}
