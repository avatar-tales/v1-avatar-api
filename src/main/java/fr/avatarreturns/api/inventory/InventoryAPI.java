package fr.avatarreturns.api.inventory;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

public interface InventoryAPI {

    /**
     * Method to change {@link Inventory}'s size
     * @param size the new size
     * @return {@link InventoryAPI}'s instance
     */
    InventoryAPI setSize(final int size);

    /**
     * Method to change {@link Inventory}'s title
     * @param name the new name
     * @return {@link InventoryAPI}'s instance
     */
    InventoryAPI setTitle(final String name);

    /**
     * Method to set if the {@link Inventory} is refreshed every 2 ticks
     * @param refreshing the {@link Boolean} value
     * @return {@link InventoryAPI}'s instance
     */
    InventoryAPI setRefresh(final boolean refreshing);

    /**
     * Method to set a function which is called every 2 ticks
     * @param function the {@link Consumer} value
     * @return {@link InventoryAPI}'s instance
     */
    InventoryAPI setFunction(final Consumer<InventoryAPI> function);

    /**
     * Method to get {@link Inventory}'s size
     * @return {@link Integer} the size
     */
    int getSize();

    /**
     * Method to get {@link Inventory}'s title
     * @return {@link String} the title
     */
    String getTitle();

    /**
     * Method to get all {@link ItemAPI} of the {@link Inventory}
     * @return {@link List} of {@link ItemAPI}
     */
    List<ItemAPI> getItems();

    /**
     * Method to get {@link InventoryAPI}'s refreshing status
     * @return {@link Boolean} the value
     */
    boolean isRefreshed();

    /**
     * Method to get {@link Consumer} executed every 2 ticks
     * @return {@link Consumer} the value
     */
    Consumer<InventoryAPI> getFunction();

    /**
     * Method to clear a slot
     * @param slot the slot
     * @return {@link InventoryAPI}'s instance
     */
    InventoryAPI clearSlot(final int slot);

    /**
     * Method to get the {@link ItemAPI} of a slot
     * @param slot the slot
     * @return {@link Optional} of an {@link ItemAPI}
     */
    Optional<ItemAPI> getItem(final int slot);

    /**
     * Method to add an {@link ItemAPI}
     * @param slot the slot
     * @param itemStack the {@link ItemStack}
     * @return {@link InventoryAPI}'s instance
     */
    InventoryAPI addItem(final int slot, final ItemStack itemStack);

    /**
     * Method to add an {@link ItemAPI}
     * @param slot the slot
     * @param function the {@link Function} to generate the {@link ItemStack}
     * @return {@link InventoryAPI}'s instance
     */
    InventoryAPI addItem(final int slot, final Function<Object, ItemStack> function);

    /**
     * Method to add an {@link ItemAPI}
     * @param slot the slot
     * @param itemStack the {@link ItemStack}
     * @param cancelled if the click event on the slot is cancelled
     * @return {@link InventoryAPI}'s instance
     */
    InventoryAPI addItem(final int slot, final ItemStack itemStack, final boolean cancelled);

    /**
     * Method to add an {@link ItemAPI}
     * @param slot the slot
     * @param function the {@link Function} to generate the {@link ItemStack}
     * @param cancelled if the click event on the slot is cancelled
     * @return {@link InventoryAPI}'s instance
     */
    InventoryAPI addItem(final int slot, final Function<Object, ItemStack> function, final boolean cancelled);

    /**
     * Method to add an {@link ItemAPI}
     * @param slot the slot
     * @param itemStack the {@link ItemStack}
     * @param cancelled if the click event on the slot is cancelled
     * @param consumer the {@link Consumer} to execute by clicking on the slot
     * @return {@link InventoryAPI}'s instance
     */
    InventoryAPI addItem(final int slot, final ItemStack itemStack, final boolean cancelled, final Consumer<InventoryClickEvent> consumer);

    /**
     * Method to add an {@link ItemAPI}
     * @param slot the slot
     * @param function the {@link Function} to generate the {@link ItemStack}
     * @param cancelled if the click event on the slot is cancelled
     * @param consumer the {@link Consumer} to execute by clicking on the slot
     * @return {@link InventoryAPI}'s instance
     */
    InventoryAPI addItem(final int slot, final Function<Object, ItemStack> function, final boolean cancelled, final Consumer<InventoryClickEvent> consumer);

    /**
     * Method to add an {@link ItemAPI}
     * @param itemAPI the {@link ItemAPI}
     * @return {@link InventoryAPI}'s instance
     */
    InventoryAPI addItem(final ItemAPI itemAPI);

    /**
     * Method to build and show the {@link Inventory} to the {@link Player}
     * @param player the player
     */
    void build(final Player player);

    /**
     * Method to stop the inventory and close it for everyone
     */
    void stop();

}
