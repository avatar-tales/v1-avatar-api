package fr.avatarreturns.api.inventory;

import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.function.Consumer;
import java.util.function.Function;

public interface ItemAPI {

    /**
     * Method to refresh the {@link ItemStack} of the {@link ItemAPI}
     * @param o an object
     */
    void refresh(final Object o);

    /**
     * Method to get the slot of the {@link ItemAPI}
     * @return the slot of the {@link ItemAPI}
     */
    int getSlot();

    /**
     * Method to get the {@link ItemStack} of the {@link ItemAPI}
     * @return the {@link ItemStack}
     */
    ItemStack getItem();

    /**
     * Method to know if the click event on the slot is cancelled
     * @return the {@link Boolean} value
     */
    boolean isCancelled();

    /**
     * Method to get the {@link Function} which is executed by clicking on the slot
     * @return the {@link Consumer} value
     */
    Consumer<InventoryClickEvent> getConsumer();

}
