package fr.avatarreturns.api.commands;

import org.bukkit.command.CommandSender;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface RegisterCommand {

    /**
     * Command like : "/commandOne test"
     * or "/commandOne" etc...
     * @return the command
     */
    String command();

    /**
     * Number of necessary arguments
     * if 0, it's just "commandOne"
     * @return the number of arguments
     */
    int parametersMin() default 0;

    /**
     * Permission to execute the command
     * @return the necessary permission
     */
    String permission() default "";

    /**
     * Command usage
     * @return the command usage
     */
    String usage() default "";

    /**
     * Command description
     * @return the command description
     */
    String description() default "An AvatarReturns's command";

    /**
     * Indicate which type of {@link CommandSender} can
     * use this command
     * @return the executor
     */
    ExecutorType executorType() default ExecutorType.ALL;

    enum ExecutorType {

        ALL(),
        PLAYERS(),
        CONSOLE()

    }

}
