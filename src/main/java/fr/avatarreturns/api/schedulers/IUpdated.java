package fr.avatarreturns.api.schedulers;

import fr.avatarreturns.api.AvatarReturnsAPI;

public interface IUpdated {

    /**
     * Method called every second.
     * Need to be registered in {@link AvatarReturnsAPI#registerUpdated(IUpdated...)}
     */
    void run();

}
