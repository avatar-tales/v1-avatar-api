package fr.avatarreturns.api;

import fr.avatarreturns.api.commands.ICommandHandler;
import fr.avatarreturns.api.commands.IDefaultCommand;
import fr.avatarreturns.api.commands.RegisterCommand;
import fr.avatarreturns.api.inventory.InventoryAPI;
import fr.avatarreturns.api.storage.databases.IDatabaseCreator;
import fr.avatarreturns.api.schedulers.IUpdated;
import fr.avatarreturns.api.users.IUser;
import fr.avatarreturns.api.storage.files.IFileManager;
import fr.avatarreturns.api.utils.IMessages;
import fr.avatarreturns.api.utils.IUtils;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Optional;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class AvatarReturnsAPI {

    private static AvatarReturnsAPI instance;
    private JavaPlugin plugin;

    protected boolean debugMode;

    public AvatarReturnsAPI(final JavaPlugin plugin) {
        if (instance != null) {
            try {
                this.finalize();
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
            return;
        }
        instance = this;
        this.plugin = plugin;
    }

    /**
     * Method to write something into the logs
     * @param level the {@link Level} of logging
     * @param log the log {@link String}
     */
    public void log(final Level level, final String log) {
        if(this.plugin == null) {
            Logger.getLogger("AvatarReturnsAPI").log(level, log);
            return;
        }
        this.log(level, log, this.plugin);
    }

    /**
     * Method to write something into the logs
     * @param level the {@link Level} of logging
     * @param log the log {@link String}
     * @param plugin the {@link JavaPlugin}
     */
    public void log(final Level level, final String log, final JavaPlugin plugin) {
        plugin.getLogger().log(level, log);
    }

    /**
     * Method to write something into the logs
     * if a constant is enabled
     * @param log the log {@link String}
     */
    public void debug(final String log) {
        if (!this.debugMode)
            return;
        if(this.plugin == null) {
            Logger.getLogger("AvatarReturnsAPI").log(Level.INFO, log);
            return;
        }
        this.plugin.getLogger().log(Level.INFO, log);
    }
    /**
     * Method to write something into the logs
     * if a constant is enabled
     * @param log the log {@link String}
     * @param plugin the {@link JavaPlugin}
     */
    public void debug(final String log, final JavaPlugin plugin) {
        if (!this.debugMode)
            return;
        plugin.getLogger().log(Level.INFO, log);
    }

    /**
     * Method to get a user from his {@link UUID}
     * @param uuid the user's {@link UUID}
     * @return {@link Optional} the user
     */
    public abstract Optional<IUser> getUser(final UUID uuid);

    /**
     * Method to get a user from the bukkit {@link Player}
     * @param player the bukkit {@link Player}
     * @return {@link Optional} the user
     */
    public Optional<IUser> getUser(final Player player) {
        return this.getUser(player.getUniqueId());
    }

    /**
     * Method to get an {@link Optional} of {@link IUser} or
     * load from his {@link UUID}
     * @param uuid {@link UUID} the user's uuid
     * @return {@link Optional}
     */
    public abstract Optional<IUser> getOrLoadUser(final UUID uuid);

    /**
     * Method to get an {@link Optional} of {@link IUser} or
     * load from his {@link OfflinePlayer}
     * @param player the {@link OfflinePlayer}
     * @return {@link Optional}
     */
    public Optional<IUser> getOrLoadUser(final OfflinePlayer player) {
        return this.getOrLoadUser(player.getUniqueId());
    }

    /**
     * Method to get an {@link IUser} without saving
     * him (USE THIS METHOD IF THE {@link Player} is offline
     * @param uuid {@link UUID} the user's uuid
     * @return {@link IUser}
     */
    public abstract IUser getUserWithoutLoadIt(final UUID uuid);

    /**
     * Method to get an {@link Optional} of {@link IUser} or
     * load from his {@link Player}
     * @param player the {@link Player}
     * @return {@link Optional}
     */
    public abstract Optional<IUser> loadUser(final Player player);

    /**
     * Method to get an {@link Optional} of {@link IUser} or
     * load from his {@link UUID}
     * @param uuid {@link UUID} the user's uuid
     * @return {@link Optional}
     */
    public abstract Optional<IUser> loadUser(final UUID uuid);

    /**
     * Method to get an {@link IUser} and remove it from the memory
     * @param user {@link IUser} the user
     * @return {@link IUser}
     */
    public abstract IUser unloadUser(final IUser user);

    /**
     * Method to get the utils object
     * @return {@link IUtils} the object
     */
    public abstract IUtils getUtils();

    /**
     * Method to register {@link IUpdated} objects
     * @param updated the {@link IUpdated} objects
     */
    public abstract void registerUpdated(final IUpdated... updated);

    /**
     * Method to unregister {@link IUpdated} objects
     * @param updated the {@link IUpdated} objects
     */
    public abstract void unregisterUpdated(final IUpdated... updated);

    /**
     * Method to check if a {@link Plugin} is supported by the
     * core plugin
     * @param plugin the {@link Plugin}'s name
     * @return {@link Boolean} the boolean value
     */
    public abstract boolean isIntegrate(final String plugin);

    /**
     * Method to get the {@link IFileManager} to use
     * easily texts files
     * @return {@link IFileManager} the file manager
     */
    public abstract IFileManager getFileManager();

    /**
     * Method to easily create commands
     * this method register methods of the class
     * @param commandHandlers class which contains {@link RegisterCommand} methods
     */
    public abstract void registerCommandClass(final ICommandHandler... commandHandlers);

    /**
     * Method to easily create commands
     * this method unregister methods of the class
     * @param commandHandlers class which contains {@link RegisterCommand} methods
     */
    public abstract void unregisterCommandClass(final ICommandHandler... commandHandlers);

    /**
     * Method to create database easily
     * @return {@link IDatabaseCreator}
     */
    public abstract IDatabaseCreator getDatabaseCreator();

    /**
     * Method to get all useful messages
     * @return {@link IMessages}
     */
    public abstract IMessages getMessages();

    /**
     * Create a {@link InventoryAPI}
     * @param plugin {@link JavaPlugin} the plugin
     * @return {@link InventoryAPI} the inventory
     */
    public abstract InventoryAPI getInventory(final JavaPlugin plugin);

    /**
     * Method to get {@link IDefaultCommand}
     * @return {@link IDefaultCommand}
     */
    public abstract IDefaultCommand getDefaultCommand();

    /**
     * Method to get the core's {@link JavaPlugin}
     * DO NOT USE THIS INSTANCE OF YOUR {@link JavaPlugin}
     * @return {@link JavaPlugin}
     */
    public JavaPlugin getPlugin() {
        return this.plugin;
    }

    /**
     * Method to return this instance
     * @return {@link AvatarReturnsAPI}
     */
    public static AvatarReturnsAPI get() {
        return instance;
    }
}
