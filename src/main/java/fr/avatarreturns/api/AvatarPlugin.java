package fr.avatarreturns.api;

import org.bukkit.plugin.java.JavaPlugin;

import java.util.Objects;

public abstract class AvatarPlugin extends JavaPlugin {

    @Override
    public void onEnable() {
        for (final String command : this.getDescription().getCommands().keySet()) {
            try {
                Objects.requireNonNull(this.getCommand(command)).setExecutor(AvatarReturnsAPI.get().getDefaultCommand());
            }
            catch (final Exception ignored) {
            }
        }
    }
}
