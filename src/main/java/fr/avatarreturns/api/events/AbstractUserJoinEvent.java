package fr.avatarreturns.api.events;

import fr.avatarreturns.api.users.IUser;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerJoinEvent;
import org.jetbrains.annotations.NotNull;

public abstract class AbstractUserJoinEvent extends Event {

    private static final HandlerList HANDLERS = new HandlerList();

    /**
     * Method to retrieve the user
     * @return {@link IUser} the user
     */
    public abstract IUser getUser();

    /**
     * Method to retrieve the original event
     * @return {@link PlayerJoinEvent} the original event
     */
    public abstract PlayerJoinEvent getEvent();

    @Override
    public @NotNull HandlerList getHandlers() {
        return HANDLERS;
    }

    public static HandlerList getHandlerList() {
        return HANDLERS;
    }
}
