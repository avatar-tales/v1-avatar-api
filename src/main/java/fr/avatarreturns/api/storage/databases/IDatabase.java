package fr.avatarreturns.api.storage.databases;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Optional;

public interface IDatabase {

    /**
     * Method to modify database asynchronously
     * @param query the query to execute
     */
    void modifyQuery(final String query);

    /**
     * Method to modify database
     * @param query the query to execute
     * @param async if the execution is asynchronous or not
     */
    void modifyQuery(final String query, final boolean async);

    /**
     * Method to read the database
     * @param query the query to execute
     * @return the {@link Optional} of {@link ResultSet} the results object
     */
    Optional<ResultSet> readQuery(final String query);

    /**
     * Method to know if a table exists
     * @param tableName the table's name
     * @return {@link Boolean} the boolean value
     */
    boolean tableExists(final String tableName);

    /**
     * Method to know if a column exists
     * @param tableName the table's name
     * @param columnName the column's name
     * @return {@link Boolean} the boolean value
     */
    boolean columnExists(final String tableName, final String columnName);

    /**
     * Method to check if the {@link Connection} is open
     * @return {@link Boolean} the boolean value
     */
    boolean isOpen();

    /**
     * Method to get the {@link Connection}
     * @return {@link Connection} the connection object
     */
    Connection getConnection();

}
