package fr.avatarreturns.api.storage.databases;

import org.bukkit.plugin.java.JavaPlugin;

public interface IDatabaseCreator {

    /**
     * Create a sqlite database
     * @param plugin your main plugin class
     * @param path the path of the database
     * @param name the name of the database
     * @return {@link IDatabase} the database
     */
    IDatabase createSQLiteDatabase(final JavaPlugin plugin, final String path, final String name);

    /**
     * Create a MySQL connection
     * @param plugin your main plugin class
     * @param host the host of the database
     * @param user the user of the database
     * @param password the password of the database
     * @param database the database name
     * @param port the port of the database
     * @return {@link IDatabase} the database
     */
    IDatabase connectMySQLDatabase(final JavaPlugin plugin, final String host, final String user, final String password, final String database, final int port);

}
