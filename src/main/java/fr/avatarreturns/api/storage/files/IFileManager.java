package fr.avatarreturns.api.storage.files;

import org.bukkit.configuration.file.YamlConfiguration;

import java.util.Optional;

public interface IFileManager {

    /**
     * Method to register a new configuration file
     * to save something (like parameters)
     * @param path the file's path
     * @param name the file's name
     * @return {@link Optional} of the config
     */
    Optional<IConfig> createFile(final String path, final String name);

    interface IConfig {

        /**
         * Method to know if it the first creation of the file
         * @return {@link Boolean} the boolean value
         */
        boolean firstCreation();

        /**
         * Method to get the bukkit configurator
         * @return {@link YamlConfiguration} the bukkit configurator
         */
        YamlConfiguration getConfig();

        /**
         * Method to save the file on the disk
         */
        void save();

        /**
         * Method to destroy forever the file
         */
        void destroy();

    }

}
