package fr.avatarreturns.api.users;

import org.bukkit.entity.Player;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface IUser {

    /**
     * Method to get the {@link IUser}'s {@link UUID}
     * @return the {@link UUID}
     */
    UUID getUniqueId();

    /**
     * Method to add some money to a {@link Player}
     * @param money the {@link Double} money to add from player's balance
     */
    void addMoney(final double money);

    /**
     * Method to remove some money to a {@link Player}
     * @param money the {@link Double} money to remove from player's balance
     */
    void removeMoney(final double money);

    /**
     * Method to get the money of the {@link Player}
     * @return {@link Double} the money of the {@link Player}
     */
    double getMoney();

    /**
     * Method to get the faction's name of the {@link Player}
     * @return {@link Optional} of the {@link String} name of the {@link Player}'s faction
     */
    Optional<String> getFactionName();

    /**
     * Method to get the bendings' name of the {@link Player}
     * @return a {@link List} of each {@link String} name of the player's bendings
     */
    List<String> getBendingName();

    /**
     * Method to get the group's name of the {@link Player}
     * @return {@link Optional} of the {@link String} name of the {@link Player}'s group
     */
    Optional<String> getGroupName();

    /**
     * Method to send the {@link Player} to an other server
     * @param name the server's name
     */
    void sendToServer(final String name);

}
